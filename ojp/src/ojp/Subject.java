package ojp;

import java.util.ArrayList;
import java.util.List;

public class Subject {
	private String name;
	private List<Mark> marks = new ArrayList<Mark>();
	
	public Subject(String name){
		this.name = name;
	}
	
	public void addMark(int value){
		Mark mark = new Mark(value);
		this.marks.add(mark);
	}
	public List<Mark> getMarks(){
		return this.marks;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
