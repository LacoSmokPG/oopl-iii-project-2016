package ojp;

abstract public class Person {
	protected String name;
	protected int age;
	
	Person(){
		this.name = "Bob";
		this.age = 11;
	}
	
	public Person(String name, int age){
		this.name = name;
		this.age = age;
	}
	public String getName(){
		return this.name;
	}
	
	public int getAge(){
		return this.age;
	}
	
	@Override
	public String toString(){
		return "Name: "+this.name+" ; Age: "+this.age;
	}
}
