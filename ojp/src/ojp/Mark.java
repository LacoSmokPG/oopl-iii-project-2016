package ojp;

public class Mark {
	private int value;
	private char[] name;
	
	public Mark(){
		this.value = 2;
	}
	
	public Mark(int value){
		this.value = value;
	}
	public int getMark(){
		return this.value;
	}
	public void setMark(int value){
		this.value = value;
	}

	public char[] getName() {
		return name;
	}

	public void setName(char[] name) {
		this.name = name;
	}
	
}
