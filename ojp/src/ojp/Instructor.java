package ojp;

public class Instructor extends Person implements Passing {
	public int threshold;
	
	Instructor(int threshold){
		this.threshold = threshold;
	}

	@Override
	public boolean letHimPass(Student student) {
		int sum;
		for (int i = 0; i < student.subjects.size(); i++ ){
			Subject buffor = student.subjects.get(i);
			sum=0;
			for (int j = 0; j < buffor.getMarks().size(); j++ ){
				Mark mark = buffor.getMarks().get(j);
				sum=sum+mark.getMark();
				
			}
			sum=sum/buffor.getMarks().size();
			
			if (sum>this.threshold){
				System.out.println("great "+ student.name +" u passed "+ buffor.getName());
			}
			else{
				return false;
			}
		}
		return true;
	}
	

}
