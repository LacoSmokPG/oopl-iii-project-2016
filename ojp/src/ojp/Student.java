package ojp;

import java.util.ArrayList;
import java.util.List;

public class Student extends Person {
	public List<Subject> subjects = new ArrayList<Subject>();
	
	public void addSubject(String value){
		Subject subject = new Subject(value);
		this.subjects.add(subject);
	}
	public List<Subject> getMarks(){
		return this.subjects;
	}
}
